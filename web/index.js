import * as wasm from "wasm-performance";

var log = document.getElementById('log');

for( var pow = 0; pow <= 6; pow ++ ) {
    var objective = Math.pow(10, pow);

    display("test for a loop of " + objective + " items <br />");
    display("========================================== <br />");

    var t0 = performance.now();
    wasm.order(objective);
    var t1 = performance.now();

    display("Rust loop takes: " + (t1 - t0) + " millisecondes. <br />");

    var t0 = performance.now();
    var i = 0;

    order(objective);

    var t1 = performance.now();

    display("Js loop takes: " + (t1 - t0) + " millisecondes.<br />");
}

function display(string) {
    var content = log.innerHTML;
    log.innerHTML = content + string;
}

function order(n) {
    var arr = [];
    for(var i = n; i > 0; i--){
      arr.push(i);
    }
    arr.sort(function(a,b){
      return a - b;
    });
    return arr;
}
