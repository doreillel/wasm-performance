mod utils;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
}

#[wasm_bindgen]
pub fn order(objective: u32) -> Vec<u32> {
    let mut i:u32 = objective;
    let mut vec: Vec<u32> = Vec::new();

    while i > 0
    {
        vec.push(i);
        i -= 1;
    }

    vec.sort();

    vec
}

