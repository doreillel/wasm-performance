After completing an online training on WebAssembly (Wasm) generated using the Rust language, I decided to conduct a test.

To determine the point at which a Wasm-generated code outperforms JavaScript code, I created two loops with an equal number of iterations.

Using the JavaScript performance object, I compared the results in my browser console.

It is noteworthy that a slight difference becomes noticeable from 10,000 iterations, and this difference rapidly increases.

How to build it:

type the command "npm install" in the folder web

build the wasm code by typing "wasm-pack build" in the root folder